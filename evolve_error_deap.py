import subprocess as sub
from time import sleep, time
from sys import exit, exc_info, argv
from numpy import random, array, insert, concatenate, power, savetxt, dsplit, hstack
from multiprocessing import Queue, Process, current_process #, cpuCount
import re
import os
import os.path
import httplib
import signal


import random


import numpy

from deap import algorithms
from deap import base
from deap import creator
from deap import tools

realworkercount=int(argv[2]) if len(argv) > 2 else  50; #determines how many requests can actually be concurrently made
base_url=argv[1] if len(argv) > 1 else "hccloudrobotics.pythonanywhere.com"

def process(request):
    connection=httplib.HTTPConnection(base_url, timeout=60);
    #print base_url+request
    try:
        connection.request('GET', request);
        data = connection.getresponse();
        #print connection.host
        if data.status == 200:
                response = data.read();
        else:   
                #try resetting the connection
                #print "bad status"
                response = ".002";
    except: 
        #print "Did not get response: ",exc_info()[0];
        response = "";
    connection.close();
    return response

def get_seed_individual(length):
    return make_random_individuals(1,length);
    #return  array([.922, 0.01, -.6568, -30.172, 0.01, 0.6])

def make_random_individuals(x,y):
    value=random.randn(x,y)*20;
    return value

def individual_get_score(gene):
    try:
        data="20 .02 "+" ".join(["%.6f"%i for i in gene]);
        response=process("/error?data="+data.replace(" ","%20"));
        error = float(response.split()[0]) if len(response)>0 else 100.;
    except: 
        print exc_info(),data
        error = 10000.;

    score = 1/power(error,5);
    score =  score if score > 0 else 0.000000000001;
    return (score,)


random.seed(3378); #set seed for the initial population
creator.create("FitnessMax", base.Fitness, weights=(1.0,))

toolbox = base.Toolbox()

# Attribute generator
#toolbox.register("attr_float", random.uniform, -20, 20)
#toolbox.register("attr_float", random.random)
toolbox.register("attr_float", random.gauss, 0, 20)

creator.create("Individual", numpy.ndarray, fitness=creator.FitnessMax)

# Structure initializers
toolbox.register("individual", tools.initRepeat, creator.Individual, toolbox.attr_float, 6)
toolbox.register("population", tools.initRepeat, list, toolbox.individual)

toolbox.register("evaluate", individual_get_score)
toolbox.register("mate", tools.cxBlend, alpha=1.5)
toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=3, indpb=0.3)
toolbox.register("select", tools.selNSGA2)
#toolbox.register("select", tools.selTournament, tournsize=20)

if __name__ == "__main__":
    CXPB, MUTPB, NGEN, NPOP = .1,.2, 20, 500
    
    pop = toolbox.population(n=NPOP)
    
    # Numpy equality function (operators.eq) between two arrays returns the
    # equality element wise, which raises an exception in the if similar()
    # check of the hall of fame. Using a different equality function like
    # numpy.array_equal or numpy.allclose solve this issue.
    hof = tools.HallOfFame(1, similar=numpy.array_equal)
    
    stats = tools.Statistics(lambda ind: ind.fitness.values)
    stats.register("avg", numpy.mean)
    stats.register("std", numpy.std)
    stats.register("min", numpy.min)
    stats.register("max", numpy.max)

    #algorithms.eaSimple(pop, toolbox, cxpb=CXPB, mutpb=MUTPB, ngen=NGEN, stats=stats, halloffame=hof)
    #algorithms.eaMuPlusLambda(pop, toolbox, 100, 200, .7, .2, NGEN, stats=stats, halloffame=hof)

    timestamp0 = time();
    
    mypops=[]
    # Evaluate the entire population
    fitnesses = map(toolbox.evaluate, pop)
    for ind, fit in zip(pop, fitnesses):
        ind.fitness.values = fit
        a = ind.tolist(); a.insert(0,fit[0])
        mypops.append(a)
    
    for g in range(NGEN):
        # Select and clone the next generation individuals
        offspring = map(toolbox.clone, toolbox.select(pop, len(pop)))
        print g
        
        # Apply crossover and mutation on the offspring
        offspring = algorithms.varAnd(offspring, toolbox, CXPB, MUTPB)
        
        # Evaluate the individuals fitness
        nextgen = [ind for ind in offspring]
        fitnesses = toolbox.map(toolbox.evaluate, nextgen)
        for ind, fit in zip(nextgen, fitnesses):
            ind.fitness.values = fit
            a = ind.tolist(); a.insert(0,fit[0])
            mypops.append(a)
        
        # The population is entirely replaced by the offspring
        pop[:] = offspring
    
    #return pop, stats, hof
    '''
    for i in range(0, 1):
        timestamp0 = time();
        #pop is the size of the population to test, totalGens is the number of mutations to go through.
        pop = 100
        gens = 100

        v=GA.Genetics(individual_get_score, make_random_individuals, get_seed_individual, 
        Nweights = 6, pop = pop, totalGens = gens, mutation_rate = .3, number_elites = int(.1*pop), number_immigrants = int(.1*pop), workercount=realworkercount);
        timestamp = time();
        output = "12c_%s-%s_%d_%d.csv" % (timestamp0,timestamp,pop,gens);
        f = open(output, "a")
        for i in mypops: savetxt(f,hstack((i,i[:,1:])),delimiter=" ");
        '''
    timestamp = time();
    output = "12c_%s-%s_%d_%d_deap.csv" % (timestamp0,timestamp,NPOP,NGEN);
    f = open(output, "a")
    for i in mypops: savetxt(f,hstack((i,i[:,1:])),delimiter=" ");
