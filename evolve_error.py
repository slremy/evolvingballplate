import subprocess as sub
from time import sleep, time
from sys import exit, exc_info, argv
from numpy import random, array, insert, concatenate, power, savetxt, dsplit, hstack
from multiprocessing import Queue, Process, current_process #, cpuCount
import re
import os
import os.path
import httplib
import signal

import GA

realworkercount=int(argv[2]) if len(argv) > 2 else  50; #determines how many requests can actually be concurrently made
base_url=argv[1] if len(argv) > 1 else "hccloudrobotics.pythonanywhere.com"

def process(request):
    connection=httplib.HTTPConnection(base_url, timeout=60);
    #print base_url+request
    try:
        connection.request('GET', request);
        data = connection.getresponse();
        #print connection.host
        if data.status == 200:
                response = data.read();
        else:   
                #try resetting the connection
                #print "bad status"
                response = ".002";
    except: 
        #print "Did not get response: ",exc_info()[0];
        response = "";
    connection.close();
    return response

def get_seed_individual(length):
    return make_random_individuals(1,length);
    #return  array([.922, 0.01, -.6568, -30.172, 0.01, 0.6])

def make_random_individuals(x,y):
    value=random.randn(x,y)*20;
    return value

def individual_get_score(gene):
    try:
        data="20 .02 "+" ".join(["%.6f"%i for i in gene]);
        response=process("/error?data="+data.replace(" ","%20"));
        error = float(response.split()[0]) if len(response)>0 else 100.;
    except: 
        print exc_info(),data
        error = 10000.;

    score = 1/power(error,5);
    score =  score if score > 0 else 0.000000000001;
    return (insert(gene, 0, score))

if __name__ == "__main__":
    for i in range(0, 1):
        timestamp0 = time();
        #pop is the size of the population to test, totalGens is the number of mutations to go through.
        pop = 100
        gens = 100

        v=GA.Genetics(individual_get_score, make_random_individuals, get_seed_individual, 
        Nweights = 6, pop = pop, totalGens = gens, mutation_rate = .3, number_elites = int(.1*pop), workercount=realworkercount);
        timestamp = time();
        output = "12b_%s-%s_%d_%d.csv" % (timestamp0,timestamp,pop,gens);
        f = open(output, "a")
        for i in v: savetxt(f,hstack((i,i[:,1:])),delimiter=" ");




