#!/usr/bin/python
#Outline for GA:
#   Generate random pop
#   while loop(
#   evaluate fitness of each chromosome
#   create new pop:
#       select 2 parents probabalistically based on fitness
#       cross over parents to get new offspring w/ crossover probability
#       mutate new offspring at each locus with mutation probability
#       place new offspring in pop
#   use new generation in algo
#   if end condition met, stop and return best solution)

from numpy import random, cumsum, arange, concatenate,zeros,argmax,mean,append,insert,array,set_printoptions
from time import sleep, time
#from multiprocessing.dummy import Pool  #threaded version..
from multiprocessing import Pool	 #processes version
from sys import exit, exc_info, argv

realworkercount = None;
make_random_individuals = None
get_seed_individual = None

#===============================================================
def GetScores(data):
    if len(data.shape) == 2: #vector of chromosomes
        pool = Pool(realworkercount);
        result = pool.map(individual_get_score, data);
        pool.close();
        pool.join();
    else:
        result = individual_get_score(data);

    return result

def Genetics(individual_get_score_f, make_random_individuals_f, get_seed_individual_f, Nweights = 6, pop = 100, totalGens = 100, mutation_rate = .2, number_elites = 10, workercount = 1):
    global realworkercount, make_random_individuals, get_seed_individual, individual_get_score;

    (realworkercount, make_random_individuals, get_seed_individual, individual_get_score) = (workercount, make_random_individuals_f, get_seed_individual_f, individual_get_score_f)

    random.seed(3378); #set seed for the initial population

    #population must be larger than 2
    chromosomes = make_random_individuals(pop, Nweights)
    seedchromosome = get_seed_individual(Nweights);

    random.seed(); #set new seed to system time

    chromosomes[-1,:]=seedchromosome
    fitnesses = GetScores(chromosomes);
    
    data = array(fitnesses);
    population_data = [None]*(totalGens+1);
    population_data[0]=data.copy();

    score = data[:,0];
    chromosomes = data[:,1:];

    #print population_data[0][:,:5], 0

    for generation in arange(totalGens)+1:
		#print time()

		#Find the best chromosome
		I = score.ravel().argsort()[-number_elites:]
		best = chromosomes[I,:];
		bestScore = score[I];

		print('Generation '+str(generation)+' Average Score= '+str(mean(score))+' Max Score= '+str(max(score)))

		#This can be parallelized. You randomly pick the two parents based on their individual fitnesses
		#   scores assumed positive
		#   was not parallelized since it does not take long to calculate for each generation.
		
		totScore = sum(score);

		prob = cumsum(score)/totScore;
		index_array = arange(pop);
		
		#intialize the index list for "mating" chromosomes
		selected = arange(2*(pop-2));  #pop-2 because you're keeping the best and introducing a random individual each generation

		for i in range(2*(pop-2)):
			testNo = 1;

			#Choose a parent
			while prob[testNo] < random.rand():
				testNo = testNo + 1;
			selected[i] = index_array[testNo];

		#Having chosen breeding population, perform mating to get next generationg
		#This can also be parallelized. You pick the two parents and then do the crossover 
		# and mutation indepently from the other parents. Only one of the two children is kept.

		for i in arange(pop-(1+number_elites)):
			chromosomeA = chromosomes[selected[i*2+0], :];
			chromosomeB = chromosomes[selected[i*2+1], :];
			n = chromosomeA.size;

			#crossover
			cross_point = int((n-1)*random.rand(1));
			chromosomeA1 = append(chromosomeA[:cross_point], chromosomeB[cross_point:n]);

			#mutation
			for j in xrange(n):
				r = random.rand(1);
				if(r > mutation_rate):
					if j == 0:
						chromosomeA1[j] = abs(chromosomeA1[j]+(random.randn(1)/100));
					else:
						chromosomeA1[j] = (chromosomeA1[j])+(random.randn(1));
			#place the new individual into the populace
			chromosomes[i, :] = chromosomeA1;

		#Add one random individual each time
		chromosomes[-number_elites-1, :] = make_random_individuals(1, Nweights);
			
		#update all but the elites (the last of the chromomes) from the map fake-reduce operator
		fitnesses = GetScores(chromosomes[:-number_elites,:]);
		data = array(fitnesses); #includes the new individuals and the new random chromosome
		chromosomes[:-number_elites, :] = data[:,1:];
		score[:-number_elites] = data[:,0];

		#update the elites
		chromosomes[-number_elites:, :] = best;
		score[-number_elites:] = bestScore;

		set_printoptions(suppress=True)
		population_data[generation] = insert(chromosomes, 0, score, axis=1);
		#print  population_data[generation][:,:5], generation
    return array(population_data);

	
if __name__ == "__main__":
	#v=Genetics(Nweights = 6, pop = 100, totalGens = 100, mutation_rate = .2,number_elites = 30)
	v=Genetics(Nweights = 6, pop = 10, totalGens = 1, mutation_rate = .2, number_elites = 1);
	timestamp = time();
	output = open(str(timestamp)+".out",'wb');
	pickle.dump(v,output);
	output.close()

