filename = "/Users/sekou/Documents/source/evolvingballplate/1479466548.27-1479466637.56_100_10.csv"
filename = "/Users/sekou/Documents/source/evolvingballplate/1481664524.25-1481665372.27_500_20.csv"
filename = "/Users/sekou/Documents/source/evolvingballplate/1481909486.79-1481910232.21_500_20.csv"
import subprocess as sub
from time import sleep, time
from sys import exit, exc_info, argv
from numpy import random, array, arange, genfromtxt
import re
import os
import os.path
import httplib
import signal
from matplotlib import use
use('TkAgg')
from matplotlib import rcParams
rcParams['ps.useafm'] = True
rcParams['pdf.use14corefonts'] = True
from matplotlib.pyplot import gca, grid, subplots_adjust,figure, xlabel, ylabel, title, savefig, show, legend, subplot, boxplot, axes, hist, savefig, xlim, ylim, plot, hist2d, axis, close as figclose
from matplotlib.colors import LogNorm

from evolve_error import *

base_url=argv[1] if len(argv) > 1 else "hccloudrobotics.pythonanywhere.com"

def get_picture(gene):
    try:
        data="20 .02 "+" ".join(["%.6f"%i for i in gene]);
        #print "/u2?data="+data.replace(" ","%20")
        response=process("/u2?data="+data.replace(" ","%20"));
    except:
        print exc_info(),data
    return response

import pylab as pl

a=genfromtxt(filename)
[popsize,numgens] = map(float,filename[:-3].split("_")[1:])
[popsize,numgens] = map(int,[popsize,numgens])
data=[]; figure(figsize=(3,15))

for j in arange(numgens):
    data.append(a[arange(popsize)+popsize*j,:])
    s=get_picture(data[-1][-9,1:])
    ss=array([map(float, i[2:].split()) for i in s.splitlines()])
    subplot(10,1,j+1);plot(ss[:,1]);ylim([-1,+1]);ylabel("Gen "+str(j+1))
    #print mean(data[-1][:,0])
    #boxplot(data[-1][:,0],positions=1)


from sklearn import neighbors
knn = neighbors.KNeighborsClassifier()
knn.fit(a[:,1:],a[:,0])


pca.fit(a[:,1:])
data=[];figure()
for j in range(int(numgens),0,-1):
    data.append(a[arange(popsize)+popsize*j,:])
    X = pca.transform(data[-1][:,1:])
    pl.scatter(X[:, 0], X[:, 1], c=data[-1][:,0],s=j*20,alpha=.5)
pl.colorbar()
        
